package com.spring.aop.revision.intro;

import com.spring.aop.revision.intro.with.WithAopBusinessComponent;
import com.spring.aop.revision.intro.without.WithoutAopBusinessComponent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class IntroMain {

    public static void main(String[] args) {

        System.out.println(" #################### START NOTE ####################");
        System.out.println("Step 01 will explain how AOP will complement the OOP to implements cross cutting concerns");
        System.out.println("First we will implement a simple logging over call method without AOP");
        System.out.println("Second we will implement a simple logging over call method with AOP");
        System.out.println(" #################### END NOTE ####################");

        ApplicationContext applicationContext = SpringApplication.run(IntroMain.class);
        // start without aop process
        // Note that we need to inject the executionTimeLoggerComponent in every business component and we have to tangling business logic with the execution time logging codes
        System.out.println(" ***** start without aop process *****");
        WithoutAopBusinessComponent withoutAopBusinessComponent = applicationContext.getBean(WithoutAopBusinessComponent.class);
        withoutAopBusinessComponent.doBusiness();
        // end without aop process
        System.out.println(" ***** end without aop process *****");
        // start with aop process
        System.out.println(" ***** start with aop process *****");
        WithAopBusinessComponent withAopBusinessComponent = applicationContext.getBean(WithAopBusinessComponent.class);
        withAopBusinessComponent.doBusiness();
        // end without aop process
        System.out.println(" ***** end with aop process *****");
    }
}
