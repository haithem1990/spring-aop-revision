package com.spring.aop.revision.intro.with;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Aspect
@Component
public class ExecutionTimeLoggerAspect {

    private static final Logger logger = LoggerFactory.getLogger(ExecutionTimeLoggerAspect.class);

    // step 03 will explain more about pointcut types
    // step 01 will use the within pointcut type to add the below code around all method in the specified package
    @Pointcut("within(com.spring.aop.revision.intro.with.*)")
    public void logExecutionTimeForAllMethodUnderDefinedPackage(){}

    // step 02 will explain more about advice types
    // step 01 will use the aroud advice type to execute aroud method that much the pointcut
    @Around("logExecutionTimeForAllMethodUnderDefinedPackage()")
    public Object log(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        try {
            return proceedingJoinPoint.proceed();
        } finally {
            long finishTime = System.currentTimeMillis();
            Duration duration = Duration.ofMillis(finishTime - startTime);
            logger.info(String.format("Duration of %s execution was %s", proceedingJoinPoint.getSignature(), duration));
        }
    }
}