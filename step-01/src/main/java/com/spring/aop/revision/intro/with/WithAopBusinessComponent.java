package com.spring.aop.revision.intro.with;

import org.springframework.stereotype.Component;

@Component
public class WithAopBusinessComponent {

    public void doBusiness(){
        System.out.println("------------------------------------ do business with aop -----------------------------------");
    }
}
