package com.spring.aop.revision.intro.without;

public class ExecutionTime {

    private String className;
    private String methodName;
    private long startTime;
    private long endTime;

    public ExecutionTime(String className, String methodName, long startTime, long endTime) {
        this.className = className;
        this.methodName = methodName;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public ExecutionTime(String className, String methodName, long startTime) {
        this.className = className;
        this.methodName = methodName;
        this.startTime = startTime;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "ExecutionTime{" +
                "className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
