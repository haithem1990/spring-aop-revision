package com.spring.aop.revision.intro.without;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ExecutionTimeLoggerComponent {

    private static final Logger logger = LoggerFactory.getLogger(ExecutionTimeLoggerComponent.class);

    public ExecutionTime start(String className, String methodName){
        return new ExecutionTime(className,methodName,System.currentTimeMillis());
    }

    public void stop(ExecutionTime executionTime){
        executionTime.setEndTime(System.currentTimeMillis());
        logger.info(executionTime.toString());
    }
}
