package com.spring.aop.revision.intro.without;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithoutAopBusinessComponent {

    private final ExecutionTimeLoggerComponent executionTimeLoggerComponent;

    @Autowired
    public WithoutAopBusinessComponent(ExecutionTimeLoggerComponent executionTimeLoggerComponent) {
        this.executionTimeLoggerComponent = executionTimeLoggerComponent;
    }

    public void doBusiness(){
        ExecutionTime executionTime = executionTimeLoggerComponent.start(this.getClass().getName(),"doBusiness");
        System.out.println("------------------------------------ do business without aop -----------------------------------");
        executionTimeLoggerComponent.stop(executionTime);
    }
}
