package com.spring.aop.revision.advice.type;

import com.spring.aop.revision.advice.type.business.after.AfterBusinessComponent;
import com.spring.aop.revision.advice.type.business.around.AroundBusinessComponent;
import com.spring.aop.revision.advice.type.business.before.BeforeBusinessComponent;
import com.spring.aop.revision.advice.type.business.returning.AfterReturningBusinessComponent;
import com.spring.aop.revision.advice.type.business.throwing.AfterThrowingBusinessComponent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class AdviceTypeMain {

    public static void main(String[] args) {

        System.out.println(" #################### START NOTE ####################");
        System.out.println("Step 02 wil provide different advice types implementations");
        System.out.println("Advice Types ---- > before, after, after returning, after throwing, around");
        System.out.println("At this step we will use the within pointcut , it will be more explained at step 03");
        System.out.println(" #################### END NOTE ####################");

        ApplicationContext applicationContext = SpringApplication.run(AdviceTypeMain.class);

        System.out.println("###### START BEFORE ADVICE TYPE ######");
        BeforeBusinessComponent beforeBusinessComponent = applicationContext.getBean(BeforeBusinessComponent.class);
        beforeBusinessComponent.doBusiness();
        System.out.println("###### END BEFORE ADVICE TYPE ######");

        System.out.println("###### START AFTER ADVICE TYPE ######");
        AfterBusinessComponent afterBusinessComponent = applicationContext.getBean(AfterBusinessComponent.class);
        afterBusinessComponent.doBusiness();
        System.out.println("###### END AFTER ADVICE TYPE ######");

        System.out.println("###### START AFTER THROWING EXCEPTION ADVICE TYPE ######");
        AfterThrowingBusinessComponent afterThrowingBusinessComponent = applicationContext.getBean(AfterThrowingBusinessComponent.class);
        afterThrowingBusinessComponent.doBusiness();
        System.out.println("###### START AFTER THROWING EXCEPTION ADVICE TYPE ######");

        System.out.println("###### START AFTER RETURNING ADVICE TYPE ######");
        AfterReturningBusinessComponent afterReturningBusinessComponent = applicationContext.getBean(AfterReturningBusinessComponent.class);
        afterReturningBusinessComponent.doBusiness();
        System.out.println("###### END AFTER RETURNING ADVICE TYPE ######");

        System.out.println("###### START AROUND ADVICE TYPE ######");
        AroundBusinessComponent aroundBusinessComponent = applicationContext.getBean(AroundBusinessComponent.class);
        aroundBusinessComponent.doBusiness();
        System.out.println("###### END AROUND ADVICE TYPE ######");
    }
}
