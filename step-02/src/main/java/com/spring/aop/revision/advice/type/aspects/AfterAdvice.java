package com.spring.aop.revision.advice.type.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AfterAdvice {

    private static final Logger logger = LoggerFactory.getLogger(AfterAdvice.class);

    // step 03 will explain more about pointcut types
    // step 02 will use the within pointcut type
    @Pointcut("within(com.spring.aop.revision.advice.type.business.after.*)")
    public void afterPackage(){}

    // add the below code after calling all methods matching afterPackage() pointcut
    @After("afterPackage()")
    public void after(){
        System.out.println(" execute code after calling method ");
    }
}