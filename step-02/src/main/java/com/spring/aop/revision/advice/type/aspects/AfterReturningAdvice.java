package com.spring.aop.revision.advice.type.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AfterReturningAdvice {

    // step 03 will explain more about pointcut types
    // step 02 will use the within pointcut type
    @Pointcut("within(com.spring.aop.revision.advice.type.business.returning.*)")
    public void returningPackage(){}

    // add the below code after returning all methods matching returningPackage() pointcut
    @AfterReturning(value = "returningPackage()", returning = "returnValue")
    public void afterReturning(JoinPoint joinPoint, Object returnValue) {
        System.out.println("after returning " + joinPoint.getSignature() + " - returnValue = " + returnValue);
        System.out.println("in this step w e can do return validation for example");
    }

}
