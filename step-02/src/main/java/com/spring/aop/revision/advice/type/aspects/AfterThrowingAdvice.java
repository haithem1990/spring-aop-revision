package com.spring.aop.revision.advice.type.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AfterThrowingAdvice {

    // step 03 will explain more about pointcut types
    // step 02 will use the within pointcut type
    @Pointcut("within(com.spring.aop.revision.advice.type.business.throwing.*)")
    public void throwingPackage(){}

    // add the below code after throwing exception in all call methods matching throwingPackage() pointcut
    @AfterThrowing(value = "throwingPackage()", throwing = "exception")
    public void afterThrowing(JoinPoint joinPoint, Exception exception) {
        System.out.println("after throwing exception - " + joinPoint.getSignature() + " - exception = " + exception);
    }

    // I add the around aspect in this part only for stopping the exception propagation
    @Around("throwingPackage()")
    public Object around(ProceedingJoinPoint joinPoint) {

        try {
            return joinPoint.proceed();
        } catch (Throwable throwable) {
            return null;
        }
    }

}
