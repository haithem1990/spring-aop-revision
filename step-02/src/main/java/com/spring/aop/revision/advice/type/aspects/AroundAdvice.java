package com.spring.aop.revision.advice.type.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AroundAdvice {
    // step 03 will explain more about pointcut types
    // step 02 will use the within pointcut type
    @Pointcut("within(com.spring.aop.revision.advice.type.business.around.*)")
    public void aroundPackage() {
    }
    // add the below code around all call methods matching aroundPackage() pointcut
    @Around("aroundPackage()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println(" execute code before calling method ");
        try {
            String result = (String) proceedingJoinPoint.proceed();
            System.out.println(" execute code after returning ");
            return result;
        } catch (Throwable throwable) {
            System.out.println(" execute code after throwing exception ");
            return "";
        } finally {
            System.out.println(" execute code after calling method ");
        }
    }

}
