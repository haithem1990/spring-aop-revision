package com.spring.aop.revision.advice.type.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BeforeAdvice {

    // step 03 will explain more about pointcut types
    // step 02 will use the within pointcut type
    @Pointcut("within(com.spring.aop.revision.advice.type.business.before.*)")
    public void beforePackage(){}
    // add the below code before all call methods matching beforePackage() pointcut
    @Before("beforePackage()")
    public void beforeAdvice(){
        System.out.println(" execute code before calling method ");
    }
}