package com.spring.aop.revision.advice.type.business.after;

import org.springframework.stereotype.Component;

@Component
public class AfterBusinessComponent {

    public void doBusiness(){
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
