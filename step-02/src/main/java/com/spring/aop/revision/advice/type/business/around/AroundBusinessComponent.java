package com.spring.aop.revision.advice.type.business.around;

import org.springframework.stereotype.Component;

@Component
public class AroundBusinessComponent {

    public void doBusiness(){
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
