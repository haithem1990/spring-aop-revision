package com.spring.aop.revision.advice.type.business.before;

import org.springframework.stereotype.Component;

@Component
public class BeforeBusinessComponent {

    public void doBusiness(){
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
