package com.spring.aop.revision.advice.type.business.returning;

import org.springframework.stereotype.Component;

@Component
public class AfterReturningBusinessComponent {

    public String doBusiness(){
        System.out.println("------------------------------------ do business-----------------------------------");
        return "done";
    }
}
