package com.spring.aop.revision.advice.type.business.throwing;

import org.springframework.stereotype.Component;

@Component
public class AfterThrowingBusinessComponent {

    public void doBusiness(){
        System.out.println("------------------------------------ do business -----------------------------------");
        throw new CustomException("oy oy , there is an exception here");
    }
}
