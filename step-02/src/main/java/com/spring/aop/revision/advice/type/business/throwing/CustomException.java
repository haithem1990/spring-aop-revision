package com.spring.aop.revision.advice.type.business.throwing;

public class CustomException extends RuntimeException {

    public CustomException(String msg){
        super(msg);
    }

}
