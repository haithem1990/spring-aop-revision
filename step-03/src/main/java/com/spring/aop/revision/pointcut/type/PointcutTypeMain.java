package com.spring.aop.revision.pointcut.type;

import com.spring.aop.revision.pointcut.type.business.annotation.AnnotationMethodBusinessComponent;
import com.spring.aop.revision.pointcut.type.business.args.ArgsBusinessComponent;
import com.spring.aop.revision.pointcut.type.business.bean.BeanBusinessComponent;
import com.spring.aop.revision.pointcut.type.business.execution.ExecutionBusinessComponent;
import com.spring.aop.revision.pointcut.type.business.within.WithinBusinessComponent;
import com.spring.aop.revision.pointcut.type.business.within.subpckg.WithinSubPckgBusinessComponent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PointcutTypeMain {
    public static void main(String[] args) {

        System.out.println(" #################### START NOTE ####################");
        System.out.println("Step 03 wil provide different pointcut types implementations");
        System.out.println("Pointcut Types ---- > @annotation, within, args, bean, execution, @within, @args, this, target, @target");
        System.out.println("At this step we will use before and after as advice types");
        System.out.println(" #################### END NOTE ####################");

        ApplicationContext applicationContext = SpringApplication.run(PointcutTypeMain.class);

        System.out.println("###### START ANNOTATION POINTCUT TYPE ######");
        AnnotationMethodBusinessComponent annotationBusinessComponent = applicationContext.getBean(AnnotationMethodBusinessComponent.class);
        annotationBusinessComponent.doBusiness();
        System.out.println("###### END ANNOTATION POINTCUT TYPE ######");

        System.out.println("###### START WITHIN POINTCUT TYPE ######");
        WithinBusinessComponent withinBusinessComponent = applicationContext.getBean(WithinBusinessComponent.class);
        withinBusinessComponent.doBusiness();
        WithinSubPckgBusinessComponent withinSubPckgBusinessComponent = applicationContext.getBean(WithinSubPckgBusinessComponent.class);
        withinSubPckgBusinessComponent.doBusiness();
        System.out.println("###### END WITHIN POINTCUT TYPE ######");

        System.out.println("###### START ARGS POINTCUT TYPE ######");
        ArgsBusinessComponent argsBusinessComponent = applicationContext.getBean(ArgsBusinessComponent.class);
        argsBusinessComponent.doBusiness("s", 1, 2, 3);
        System.out.println("###### END ARGS POINTCUT TYPE ######");

        System.out.println("###### START BEAN POINTCUT TYPE ######");
        BeanBusinessComponent beanBusinessComponent = applicationContext.getBean(BeanBusinessComponent.class);
        beanBusinessComponent.doBusiness();
        System.out.println("###### END BEAN POINTCUT TYPE ######");

        System.out.println("###### START EXECUTION POINTCUT TYPE ######");
        ExecutionBusinessComponent executionBusinessComponent = applicationContext.getBean(ExecutionBusinessComponent.class);
        executionBusinessComponent.doBusiness();
        System.out.println("###### END EXECUTION POINTCUT TYPE ######");



    }
}
