package com.spring.aop.revision.pointcut.type.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AnnotationPointcutAspect {

    @Pointcut("@annotation(com.spring.aop.revision.pointcut.type.business.annotation.CustomMethodAnnotation)")
    public void executeTargetMethod(){}

    @Before("executeTargetMethod()")
    public void beforeAdvice(){
        System.out.println(" execute code before calling annotated method ");
    }

    @After("executeTargetMethod()")
    public void afterAdvice(){
        System.out.println(" execute code after calling annotated method ");
    }

    // we can do the other advice types like around , after throwing and after returning
}