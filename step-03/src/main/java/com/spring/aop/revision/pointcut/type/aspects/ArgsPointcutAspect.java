package com.spring.aop.revision.pointcut.type.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ArgsPointcutAspect {

    // arg(..) --> match any parameters
    // arg(parameter_type_1,..) --> define only the first parameter type and should have more than two other parameters
    // arg(parameter_type_1,*) --> define only the first parameter type and should have an other parameter
    // arg(parameter_type_1,parameter_type_2,..,parameter_type_n) --> define the first ,the second and the final parameters and tolerate others parameters
    @Pointcut("args(java.lang.String,..,int)")
    public void executeTargetMethod() {
    }

    @Before("executeTargetMethod()")
    public void beforeAdvice() {
        System.out.println(" execute code before calling target method ");
    }

    @After("executeTargetMethod()")
    public void afterAdvice() {
        System.out.println(" execute code after calling target method ");
    }

    // we can do the other advice types like around , after throwing and after returning
}