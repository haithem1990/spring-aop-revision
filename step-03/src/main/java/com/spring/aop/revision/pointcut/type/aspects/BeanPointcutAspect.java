package com.spring.aop.revision.pointcut.type.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BeanPointcutAspect {

    // bean(bean*) --> match the Spring bean name that start with bean as prefix
    // bean(bean*Component) --> match the Spring bean name that start with bean as prefix and finish with Component as suffix
    // bean(beanName) --> match the Spring bean name
    @Pointcut("bean(bean*)")
    public void executeTargetMethod() {
    }

    @Before("executeTargetMethod()")
    public void beforeAdvice() {
        System.out.println(" execute code before calling target method ");
    }

    @After("executeTargetMethod()")
    public void afterAdvice() {
        System.out.println(" execute code after calling target method ");
    }

    // we can do the other advice types like around , after throwing and after returning
}