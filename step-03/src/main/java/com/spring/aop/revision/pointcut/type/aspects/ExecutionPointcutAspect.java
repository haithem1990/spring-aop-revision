package com.spring.aop.revision.pointcut.type.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ExecutionPointcutAspect {

    // execution([visibility_modifiers] [return_types] package.class.method([arguments]) [throws_exception])

    // [visibility_modifiers] ---> public/protected , if omitted all are matched
    // [return_types] ---> void, primitive, Object Type, cannot be omitted , * for all matching, ! to eliminate
    // [package] ---> target package, cannot be omitted , * for all matching
    // [class] ---> target class, can be omitted , * for all matching

    @Pointcut("execution(void com.spring.aop.revision.pointcut.type.business.execution.ExecutionBusinessComponent.do*())")
    public void executeTargetMethod(){}

    @Before("executeTargetMethod()")
    public void beforeExecute(){
        System.out.println(" execute code before calling target method ");
    }
}