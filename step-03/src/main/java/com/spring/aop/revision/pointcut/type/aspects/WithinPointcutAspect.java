package com.spring.aop.revision.pointcut.type.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class WithinPointcutAspect {

    // com.spring.aop.revision.pointcut.type.business.within.WithinBusinessComponent --> com.spring.aop.revision.pointcut.type.business.within.WithinBusinessComponent class
    // com..*.within..* --> all within package and sub packages under com
    // com.spring.aop.revision.pointcut.type.business.within.* --> com.spring.aop.revision.pointcut.type.business.within package but without sub packages
    // com.spring.aop.revision.pointcut.type.business.within..* --> com.spring.aop.revision.pointcut.type.business.within package and all sub packages
    @Pointcut("within(com.spring.aop.revision.pointcut.type.business.within..*)")
    public void executeTargetMethod(){}

    @Before("executeTargetMethod()")
    public void beforeWithinPointcut(){
        System.out.println(" execute code before calling method in the pointcut package");
    }

    @After("executeTargetMethod()")
    public void afterWithinPointcut(){
        System.out.println(" execute code after calling method in the pointcut package ");
    }

    // we can do the other advice types like around , after throwing and after returning
}