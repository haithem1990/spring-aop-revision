package com.spring.aop.revision.pointcut.type.business.annotation;

import org.springframework.stereotype.Component;

@Component
public class AnnotationMethodBusinessComponent {

    @CustomMethodAnnotation
    public void doBusiness(){
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
