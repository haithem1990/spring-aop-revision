package com.spring.aop.revision.pointcut.type.business.annotation;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
public @interface CustomMethodAnnotation {
}
