package com.spring.aop.revision.pointcut.type.business.args;

import org.springframework.stereotype.Component;

@Component
public class ArgsBusinessComponent {

    public void doBusiness(String str, double d, long l, int i) {
        System.out.println("------------------------------------ do business -----------------------------------");
    }


}
