package com.spring.aop.revision.pointcut.type.business.bean;

import org.springframework.stereotype.Component;

@Component
public class BeanBusinessComponent {

    public void doBusiness() {
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
