package com.spring.aop.revision.pointcut.type.business.execution;

import org.springframework.stereotype.Component;

@Component
public class ExecutionBusinessComponent {

    public void doBusiness() {
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
