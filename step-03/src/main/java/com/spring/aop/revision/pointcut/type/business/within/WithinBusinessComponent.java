package com.spring.aop.revision.pointcut.type.business.within;

import org.springframework.stereotype.Component;

@Component
public class WithinBusinessComponent {

    public void doBusiness() {
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
