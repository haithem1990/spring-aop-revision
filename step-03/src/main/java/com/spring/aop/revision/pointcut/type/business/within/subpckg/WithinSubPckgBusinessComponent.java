package com.spring.aop.revision.pointcut.type.business.within.subpckg;

import org.springframework.stereotype.Component;

@Component
public class WithinSubPckgBusinessComponent {

    public void doBusiness() {
        System.out.println("------------------------------------ do business -----------------------------------");
    }
}
