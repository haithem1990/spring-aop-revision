package com.spring.aop.revision.resume;

import com.spring.aop.revision.resume.business.end.Version1BusinessComponent;
import com.spring.aop.revision.resume.business.end.Version2BusinessComponent;
import com.spring.aop.revision.resume.business.self.SelfCallBusinessComponent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ResumeMain {

    public static void main(String[] args) {

        System.out.println(" #################### START NOTE ####################");
        System.out.println(" #################### END NOTE ####################");

        ApplicationContext applicationContext = SpringApplication.run(ResumeMain.class);

        System.out.println(" #################### AOP DON'T SUPPORT SELF CALL ####################");

        SelfCallBusinessComponent selfCallBusinessComponent = applicationContext.getBean(SelfCallBusinessComponent.class);

        selfCallBusinessComponent.doBusiness();

        System.out.println(" -------> the aop executed with selfCall() in doBusiness()  method in SelfCallBusinessComponent.class is not called");

        selfCallBusinessComponent.selfCall();

        System.out.println(" -------> the aop executed with selfCall() under SelfCallBusinessComponent.class is called , because when we call the method out of class , we use the proxy class ");

        System.out.println(" #################### END AOP DON'T SUPPORT SELF CALL ####################");

        System.out.println(" #################### RESUME ####################");

        System.out.println(" #################### WRITE AN AOP THAT AROUND A METHOD STARTED WITH SEND AND ANNOTATED WITH A CUSTOM ANNOTATION ####################");

        System.out.println(" #################### SOLUTION V1 ####################");

        Version1BusinessComponent version1BusinessComponent = applicationContext.getBean(Version1BusinessComponent.class);

        version1BusinessComponent.sendData();

        System.out.println(" #################### SOLUTION V2 ####################");

        Version2BusinessComponent version2BusinessComponent = applicationContext.getBean(Version2BusinessComponent.class);

        version2BusinessComponent.sendData();

        System.out.println(" #################### THE END  ####################");


    }
}
