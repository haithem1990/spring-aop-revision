package com.spring.aop.revision.resume.aspects.self;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SelfCallAspect {

    @Pointcut("execution(void com.spring.aop.revision.resume.business.self.SelfCallBusinessComponent.do*())")
    public void startedWithDo(){
    }

    @Pointcut("execution(void com.spring.aop.revision.resume.business.self.SelfCallBusinessComponent.self*())")
    public void startedWithSelf(){
    }

    @Before("startedWithDo()")
    public void beforeExecuteMethodStartedWithDo(){
        System.out.println(" execute code before calling method started with do");
    }

    @Before("startedWithSelf()")
    public void beforeExecuteMethodStartedWithSelf(){
        System.out.println(" execute code before calling method started with self");
    }

}
