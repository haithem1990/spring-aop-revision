package com.spring.aop.revision.resume.aspects.v1;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class V1Aspect {


    @Pointcut("execution(@com.spring.aop.revision.resume.business.end.CustomAnnotation * com.spring.aop.revision.resume.business.end.Version1BusinessComponent.send*(..))")
    public void callMethodStartedWithSendAndAnnotatedWithCustomAnnotation(){}

    @Before("callMethodStartedWithSendAndAnnotatedWithCustomAnnotation()")
    public void beforeExecute(){
        System.out.println("v1 ---> before ");
    }

    @After("callMethodStartedWithSendAndAnnotatedWithCustomAnnotation()")
    public void afterExecute(){
        System.out.println("v1 ---> after ");
    }

    @AfterReturning(value = "callMethodStartedWithSendAndAnnotatedWithCustomAnnotation()",returning = "response")
    public void afterReturningExecute(JoinPoint joinPoint,String response){
        System.out.println("v1 ---> after returning response = " + response);
    }

    @AfterThrowing("callMethodStartedWithSendAndAnnotatedWithCustomAnnotation()")
    public void afterThrowingExecute(){
        System.out.println("v1 ---> after throwing ");
    }
}
