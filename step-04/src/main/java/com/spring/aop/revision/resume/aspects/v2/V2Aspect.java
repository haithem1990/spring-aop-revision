package com.spring.aop.revision.resume.aspects.v2;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class V2Aspect {


    @Pointcut("execution(@com.spring.aop.revision.resume.business.end.CustomAnnotation * com.spring.aop.revision.resume.business.end.Version2BusinessComponent.send*(..))")
    public void callMethodStartedWithSendAndAnnotatedWithCustomAnnotation() {
    }

    @Around("callMethodStartedWithSendAndAnnotatedWithCustomAnnotation()")
    public void aroundExecute(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        System.out.println("v2 ---> before");
        try {
            String response = (String) proceedingJoinPoint.proceed();
            System.out.println("v2 ---> after returning response = " + response);
        } catch (Throwable throwable) {
            System.out.println("v2 ---> after Throwing");
            throw throwable;
        } finally {
            System.out.println("v2 ---> after");
        }
    }
}
