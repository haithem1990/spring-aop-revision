package com.spring.aop.revision.resume.business.end;

import org.springframework.stereotype.Component;

@Component
public class Version1BusinessComponent {

    @CustomAnnotation
    public String sendData(){
        System.out.println("//////////////////// sendData /////////////////////////");
        return "ok";
    }
}
