package com.spring.aop.revision.resume.business.end;

import org.springframework.stereotype.Component;

@Component
public class Version2BusinessComponent {

    @CustomAnnotation
    public String sendData(){
        System.out.println("//////////////////// sendData /////////////////////////");
        return "ok";
    }
}
