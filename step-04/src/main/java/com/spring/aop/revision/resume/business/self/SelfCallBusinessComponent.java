package com.spring.aop.revision.resume.business.self;

import org.springframework.stereotype.Component;

@Component
public class SelfCallBusinessComponent {

    public void doBusiness(){
        System.out.println("------------------------------------ do business -----------------------------------");
        // self call is not supported with aop
        // ie --> any aop trigered when we call a self method is not executed
        // when we call a selfCall method we use the target object
        // the aop is executed only when we call the method via the proxy
        selfCall();
    }

    public void selfCall(){
        System.out.println("------------------------------------ self call-----------------------------------");
    }
}
